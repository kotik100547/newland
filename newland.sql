-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 26 2019 г., 00:30
-- Версия сервера: 10.1.10-MariaDB
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `newland`
--

-- --------------------------------------------------------

--
-- Структура таблицы `priorities`
--

CREATE TABLE `priorities` (
  `priority_id` int(11) NOT NULL,
  `priority_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `priorities`
--

INSERT INTO `priorities` (`priority_id`, `priority_name`) VALUES
(1, 'Некритично'),
(2, 'Условно некритично'),
(3, 'Критично'),
(4, 'Максимально критично');

-- --------------------------------------------------------

--
-- Структура таблицы `tickets`
--

CREATE TABLE `tickets` (
  `version_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `assignedto` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `starttime` datetime NOT NULL,
  `esttime` datetime DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `fd` datetime NOT NULL,
  `td` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tickets`
--

INSERT INTO `tickets` (`version_id`, `ticket_id`, `subject`, `author`, `assignedto`, `content`, `starttime`, `esttime`, `priority_id`, `fd`, `td`) VALUES
(0, 0, 'ПРоверка', 'Алексей', 'Доктор Хаус', 'ПРОВЕРКА 1', '2019-03-13 00:00:00', '2019-03-15 00:00:00', 1, '0000-00-00 00:00:00', NULL),
(0, 0, 'Проверка 2', 'Ваня', 'Антон', 'Проверка 2', '2019-03-09 00:00:00', '2019-03-24 00:00:00', 4, '0000-00-00 00:00:00', NULL),
(0, 0, '', NULL, NULL, '', '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00', NULL),
(0, 0, 'ПРоверка 4', 'Ваня', 'Антон', 'ПРоверка 4', '2019-03-27 00:00:00', '2019-03-03 00:00:00', 3, '0000-00-00 00:00:00', NULL),
(0, 0, '12', 'серж', 'игорь', '12', '2019-03-07 00:00:00', '2019-03-13 00:00:00', 2, '0000-00-00 00:00:00', NULL),
(0, 0, 'проверка6', 'серж', 'игорь', 'проверка6', '2019-03-14 00:00:00', '2019-03-07 00:00:00', 2, '0000-00-00 00:00:00', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`priority_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `priorities`
--
ALTER TABLE `priorities`
  MODIFY `priority_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
