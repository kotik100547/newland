<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">

   <!-- <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>-->

    <div class="body-content">
        <div class="newland">

            <div class="newland-top">
                <ul>
                    <li>
                        <div class="top-1">
                            <a href="#">Cистема заявок</a>
                                <a href="#">Wiki</a>
                        </div>
                    </li>
                    <li>
                        <div class="top-2">
                            <a href="#">Фамилия И.О.</a>
                            <a href="#">Выход</a>
                        </div>
                    </li>
                    <li><img src="img/newland.png"/></li>
                </ul>

            </div>
            <div class="newland-content">
                <div class="content">
                    <?php $form = ActiveForm::begin(['action' => ['site/save'],'options' => ['method' => 'post']]); ?>
<!--                    <form name="form" method="post" action="--><?php //echo Url::to(['site/save']) ?><!--">-->
                    <div class="content-top">
                        <div class="content-zajavka">
                            <h4>ЗАЯВКА #23342</h4>
                        </div>

                            <?= $form->field($tickets, 'subject') ?>


                        <?= $form->field($tickets,  'content')->textarea(['rows' => 6]) ?>

                     </div>

                     <div class="content-footer">
                         <div class="footer-author">
                            <ul>

                                <li><?= $form->field($tickets, 'starttime')->input('date') ?> </li>
                                <li><?= $form->field($tickets, 'author') ?> </li>
                                <li><?= $form->field($tickets, 'priority_id')->dropdownList($model) ?></li>

                           </ul>
                        </div>
                        <div class="footer-performer">
                            <ul>

                                <li><?= $form->field($tickets, 'esttime')->input('date') ?> </li>

                                <li><?= $form->field($tickets, 'assignedto') ?> </li>
                                <li><label>Информировать:*</label></li>
                                <li><input type="button" onclick="info();"  value="+"/></li>
                                <li id="block-1">

                                    <a href="">ФИО Сотрудника</a>

                                </li>
                            </ul>
                        </div>



                        </div>
                        <div class="footer-find-more">
                        <div class="footer-find">
                                <ul>
                                    <li>
                                        <div class="footer-find-1">
                                            <img src="img/skrepka.png" width="40px" height="40px"/>
                                            <input type="file"/>
                                    </li>
                                    <li>
                                        <?= Html::submitButton('Отправить', ['name' => 'button']) ?>
                                    </li>
                                </ul>
                            </div>
                      <div class="footer-last">
                          <a id="addFile" onclick="addFile();" href="#">Добавить еще файл</a>
                      </div>
                        </div>
                    </div>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
<!--                    </form>-->
                <?php ActiveForm::end(); ?>
                </div>

            </div>






    </div>
<!--
        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>-->
</div>
