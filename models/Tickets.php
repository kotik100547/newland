<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 24.3.19
 * Time: 20.24
 */

namespace app\models;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "product".
 *


 * @property string $subject
 * @property string $content
 * @property datetime $starttime
 * @property datetime $esttime
 * @property string $author
 * @property string $assignedto
 * @property int $priority_id

 */

class Tickets extends ActiveRecord
{


    public static function tableName()
    {
        return 'tickets';
    }

    public function rules()
    {
        return [
            [['subject', 'content', 'starttime' , 'esttime' , 'author' , 'assignedto' , 'priority_id'], 'required'],
            [['subject', 'author', 'assignedto'], 'string', 'max' => 255],
            [['priority_id'], 'integer'],
//            [['starttime' , 'esttime'], 'date','format' => 'Y-m-d'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'subject' => "Тема заявки",
            'content' => "Содержание",
            'starttime' => "Дата начала",
            'esttime' => "Планируемая дата выполнения",
            'author' => "Автор",
            'assignedto' => "Исполнитель",
           'priority_id' => "Приоритет*:",
        ];
    }

}